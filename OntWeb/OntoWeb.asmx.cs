﻿using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using ElasticSearchNestConnector;
using Nest;
using Elasticsearch.Net.Connection;

namespace OntWeb
{

    /// <summary>
    /// Summary description for OntoWeb
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class OntoWeb : System.Web.Services.WebService
    {

        private clsDBSelector dbSelector;
        private clsDBUpdater dbUpdater;
        private clsDBDeletor dbDeletor;

        private string session;

        clsTypes types = new clsTypes();
        clsFields fields = new clsFields();
        clsClassTypes classTypes = new clsClassTypes();

        public OntoWeb()
        {
            var OItem_Result = Globals.LoadConfig();
            if (OItem_Result.GUID == Globals.LogStates.LogState_Success.GUID)
            {

                dbSelector = new clsDBSelector(Globals.ElServer, Globals.ElPort, Globals.ElIndex, Globals.RepIndex, Globals.ElSearchRange, Globals.NewGuid);
                dbUpdater = new clsDBUpdater(dbSelector);
                dbDeletor = new clsDBDeletor(dbSelector);
            }
            else
            {
                SoapException se = new SoapException("Config-Error", SoapException.ClientFaultCode);
                throw se;
            }
        }

        

        [WebMethod]
        public WebServiceResult ObjectAtts(List<clsObjectAtt> oList_ObjAttributes, bool onlyIds, bool doCount = false)
        {
            
            try
            {
                
                var result = new WebServiceResult();

                if (doCount)
                {
                    var itemCount = dbSelector.get_Data_ObjectAttCount(oList_ObjAttributes);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), Count = itemCount };
                }
                else
                {
                    var oList = dbSelector.get_Data_ObjectAtt(oList_ObjAttributes, onlyIds);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), ObjectAttributes = oList };
                }
                
                return result;
            }catch (Exception ex)
            {

                var oResult = Globals.LogStates.LogState_Error.Clone();
                oResult.Additional1 = ex.Message;
                var result = new WebServiceResult { Result = oResult};
                return result;
            }
            

        }

        [WebMethod]
        public WebServiceResult ClassAttributes(List<clsOntologyItem> oList_Classes, List<clsOntologyItem> oList_AttributeTypes, bool onlyIds, bool doCount = false)
        {

            try
            {
                
                var result = new WebServiceResult();
                if (doCount)
                {
                    var countItem = dbSelector.get_Data_ClassAttCount(oList_Classes, oList_AttributeTypes);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), Count = countItem };
                }
                else
                {
                    var oList = dbSelector.get_Data_ClassAtt(oList_Classes, oList_AttributeTypes, onlyIds);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), ClassAttributes = oList };
                }
                
                return result;
            }
            catch (Exception ex)
            {
                var oResult = Globals.LogStates.LogState_Error.Clone();
                oResult.Additional1 = ex.Message;
                var result = new WebServiceResult { Result = oResult };
                return result;
            }
            
            

        }

        [WebMethod]
        public WebServiceResult ClassRelations(List<clsClassRel> oList_ClassRel, bool onlyIds, bool queryOr = true, bool doCount = false)
        {
            try
            {
                
                var result = new WebServiceResult();
                if (doCount)
                {
                    var countItem = dbSelector.get_Data_ClassRelCount(oList_ClassRel);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), Count = countItem };
                }
                else
                {
                    var oList = dbSelector.get_Data_ClassRel(oList_ClassRel, onlyIds, queryOr);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), ClassRelations = oList };
                }
                
                return result;
            }
            catch (Exception ex)
            {
                var oResult = Globals.LogStates.LogState_Error.Clone();
                oResult.Additional1 = ex.Message;
                var result = new WebServiceResult { Result = oResult };
                return result;
            }
            
        }

        [WebMethod]
        public string Type_Object()
        {
            return types.ObjectType;
        }

        [WebMethod]
        public string Type_Class()
        {
            return types.ClassType;


        }

        [WebMethod]
        public string Type_AttributeType()
        {
            return types.AttributeType;
        }

        [WebMethod]
        public string Type_RelationType()
        {
            return types.RelationType;
        }

        [WebMethod]
        public string Type_DataType()
        {
            return types.DataType;
        }

        [WebMethod]
        public string Type_ObjectAttribute()
        {
            return types.ObjectAtt;
        }

        [WebMethod]
        public string Type_ObjectRelation()
        {
            return types.ObjectRel;
        }

        [WebMethod]
        public string Type_Other()
        {
            return types.Other;
        }

        [WebMethod]
        public string Type_Other_AttType()
        {
            return types.Other_AttType;
        }

        [WebMethod]
        public string Type_Other_Classes()
        {
            return types.Other_Classes;
        }

        [WebMethod]
        public string Type_Other_RelType()
        {
            return types.Other_RelType;
        }

        [WebMethod]
        public string Type_ClassAtt()
        {
            return types.ClassAtt;
        }

        [WebMethod]
        public string Type_ClassRel()
        {
            return types.ClassRel;
        }

        [WebMethod]
        public clsLogStates OLogStates()
        {
            var result = new clsLogStates();
            return result;
        }

        [WebMethod]
        public clsMappingRules OMappingRules()
        {
            return new clsMappingRules();
        }

        [WebMethod]
        public clsOntologyRelationRules ORelationRules()
        {
            return new clsOntologyRelationRules();
        }

        [WebMethod]
        public clsClasses OClasses()
        {
            return new clsClasses();
        }

        [WebMethod]
        public string ClassType_ClassAtt()
        {
            return classTypes.ClassType_ClassAtt;
        }

        [WebMethod]
        public string ClassType_ClassRel()
        {
            return classTypes.ClassType_ClassRel;
        }

        [WebMethod]
        public string ClassType_ObjectAtt()
        {
            return classTypes.ClassType_ObjectAtt;
        }

        [WebMethod]
        public string ClassType_ObjectRel()
        {
            return classTypes.ClassType_ObjectRel;
        }

        [WebMethod]
        public string ClassType_OntologyItem()
        {
            return classTypes.ClassType_OntologyItem;
        }

        [WebMethod]
        public clsAttributeTypes OAttributeTypes()
        {
            return new clsAttributeTypes();
        }

        [WebMethod]
        public clsRelationTypes ORelationTypes()
        {
            return new clsRelationTypes();
        }

        [WebMethod]
        public clsBaseClassAttributes OClassAttributes()
        {
            return new clsBaseClassAttributes();
        }

        [WebMethod]
        public clsBaseClassRelation OClassRelatations()
        {
            return new clsBaseClassRelation();
        }

        [WebMethod]
        public clsDataTypes ODataTypes()
        {
            return new clsDataTypes();
        }

        [WebMethod]
        public clsVariables OVariables()
        {
            return new clsVariables();
        }

        [WebMethod]
        public clsDirections ODirections()
        {
            return new clsDirections();
        }

        [WebMethod]
        public clsTypes OTypes()
        {
            var result = new clsTypes();
            return result;
        }

        [WebMethod]
        public List<Config> Config()
        {

            return Globals.Config;
        }

        [WebMethod]
        public string RegExGuid()
        {
            return Globals.RegEx_Guid;
        }

        [WebMethod]
        public WebServiceResult ObjectRels(List<clsObjectRel> oLIst_ObjRel, bool onlyIds, bool doCount = false)
        {
            try
            {
                
                var result = new WebServiceResult();

                if (doCount)
                {
                    var itemCount = dbSelector.get_Data_ObjectRelCount(oLIst_ObjRel);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), Count = itemCount };
                }
                else
                {
                    var oList = dbSelector.get_Data_ObjectRel(oLIst_ObjRel, onlyIds);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), ObjectRelations = oList };
                }
                
                return result;
            }
            catch (Exception ex)
            {
                var oResult = Globals.LogStates.LogState_Error.Clone();
                oResult.Additional1 = ex.Message;
                var result = new WebServiceResult { Result = oResult };
                return result;
            }
        }

        [WebMethod]
        public WebServiceResult Objects(List<clsOntologyItem> oList_Objects, bool doCount=false)
        {
            try
            {
                
                var result = new WebServiceResult();

                if (doCount)
                {
                    var itemCount = dbSelector.get_Data_ObjectsCount(oList_Objects);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), Count = itemCount };
                }
                else
                {
                    var oList = dbSelector.get_Data_Objects(oList_Objects);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), OntologyItems = oList };
                }
                
                return result;
            }
            catch (Exception ex)
            {
                var oResult = Globals.LogStates.LogState_Error.Clone();
                oResult.Additional1 = ex.Message;
                var result = new WebServiceResult { Result = oResult };
                return result;
            }

            
        }

        [WebMethod]
        public WebServiceResult Classes(List<clsOntologyItem> oList_Classes, bool doCount = false)
        {
            try
            {
                
                var result = new WebServiceResult();
                if (doCount)
                {
                    var countItem = dbSelector.get_Data_ClassesCount(oList_Classes);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), Count = countItem };
                }
                else
                {
                    var oList = dbSelector.get_Data_Classes(oList_Classes);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), OntologyItems = oList };
                }
                
                return result;
            }
            catch (Exception ex)
            {
                var oResult = Globals.LogStates.LogState_Error.Clone();
                oResult.Additional1 = ex.Message;
                var result = new WebServiceResult { Result = oResult };
                return result;
            }

        }

        [WebMethod]
        public WebServiceResult RelationTypes(List<clsOntologyItem> oLIst_RelTypes, bool doCount = false)
        {
            try
            {
                
                if (doCount)
                {
                    var itemCount = dbSelector.get_Data_RelationTypesCount(oLIst_RelTypes);
                    var result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), Count = itemCount };
                    return result;
                }
                else
                {
                    var oList = dbSelector.get_Data_RelationTypes(oLIst_RelTypes);
                    var result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), OntologyItems = oList };
                    return result;
                }
                
            }
            catch (Exception ex)
            {
                var oResult = Globals.LogStates.LogState_Error.Clone();
                oResult.Additional1 = ex.Message;
                var result = new WebServiceResult { Result = oResult };
                return result;
            }

        }

        [WebMethod]
        public WebServiceResult ObjectTree(clsOntologyItem objOItem_Class_Par, clsOntologyItem objOitem_Class_Child,
                                          clsOntologyItem objOItem_RelationType, bool doCount = false)
        {
            try
            {
                
                var result = new WebServiceResult();
                if (doCount)
                {
                    var itemCount = dbSelector.get_Data_Objects_Tree_Count(objOItem_Class_Par, objOitem_Class_Child, objOItem_RelationType);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), Count = itemCount };
                }
                else
                {
                    var oList = dbSelector.get_Data_Objects_Tree(objOItem_Class_Par, objOitem_Class_Child, objOItem_RelationType);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), ObjectTrees = oList };
                    result.OntologyItems1 = dbSelector.OntologyList_Objects1;
                    result.OntologyItems2 = dbSelector.OntologyList_Objects2;
                }

                return result;
            }
            catch (Exception ex)
            {
                var oResult = Globals.LogStates.LogState_Error.Clone();
                oResult.Additional1 = ex.Message;
                var result = new WebServiceResult { Result = oResult };
                return result;
            }
        }

        [WebMethod]
        public bool IndexExists(string index)
        {
            var objIndexDescriptor = dbSelector.GetIndexExistsDescriptor();
            objIndexDescriptor.Index(index);

            return dbSelector.ElConnector.IndexExists(f => objIndexDescriptor).Exists;
        }

        [WebMethod]
        public WebServiceResult DataTypes(List<clsOntologyItem> oList_DataTypes, bool doCount = false)
        {
            try
            {

                var result = new WebServiceResult();
                if (doCount)
                {
                    var countItem = dbSelector.get_Data_DataTypesCount(oList_DataTypes);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), Count = countItem };
                }
                else
                {
                    var oList = dbSelector.get_Data_DataTypes(oList_DataTypes);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), OntologyItems = oList };
                }

                return result;
            }
            catch (Exception ex)
            {
                var oResult = Globals.LogStates.LogState_Error.Clone();
                oResult.Additional1 = ex.Message;
                var result = new WebServiceResult { Result = oResult };
                return result;
            }

        }

        [WebMethod]
        public WebServiceResult ObjectAttributesOrderId(clsOntologyItem oItem_Object, clsOntologyItem oItem_AttributeType, string sortField, bool doAsc)
        {
            try
            {

                var result = new WebServiceResult();
                var lngOrderID = dbSelector.get_Data_Att_OrderByVal(sortField, oItem_Object, oItem_AttributeType, doAsc);
                result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), OrderId = lngOrderID };
                
                return result;
            }
            catch (Exception ex)
            {
                var oResult = Globals.LogStates.LogState_Error.Clone();
                oResult.Additional1 = ex.Message;
                var result = new WebServiceResult { Result = oResult };
                return result;
            }

        }

        [WebMethod]
        public WebServiceResult ObjectRelationsOrderId(clsOntologyItem oItem_Left = null,
                                         clsOntologyItem oItem_Right = null, 
                                         clsOntologyItem oItem_RelationType = null,
                                         bool doASC = true)
        {
            try
            {

                var result = new WebServiceResult();
                var lngOrderID = dbSelector.get_Data_Rel_OrderByVal(oItem_Left,
                                                           oItem_Right,
                                                           oItem_RelationType,
                                                           "OrderID",
                                                           doASC);
                result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), OrderId = lngOrderID };

                return result;
            }
            catch (Exception ex)
            {
                var oResult = Globals.LogStates.LogState_Error.Clone();
                oResult.Additional1 = ex.Message;
                var result = new WebServiceResult { Result = oResult };
                return result;
            }

        }

        [WebMethod]
        public WebServiceResult ObjectAttributesOrderByVal(string strOrderField, 
                                     clsOntologyItem oItem_Object = null,
                                         clsOntologyItem oItem_AttributeType = null,
                                         bool doAsc = true)
        {
            try
            {

                var result = new WebServiceResult();

                var lngOrderID = dbSelector.get_Data_Att_OrderByVal(strOrderField, oItem_Object, oItem_AttributeType, doAsc);
                result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), OrderId = lngOrderID };

                return result;
            }
            catch (Exception ex)
            {
                var oResult = Globals.LogStates.LogState_Error.Clone();
                oResult.Additional1 = ex.Message;
                var result = new WebServiceResult { Result = oResult };
                return result;
            }

        }

        [WebMethod]
        public WebServiceResult AttributeTypes(List<clsOntologyItem> oList_AttType, bool doCount = false)
        {
            try
            {
                
                var result = new WebServiceResult();
                if (doCount)
                {
                    var countItem = dbSelector.get_Data_AttributeTypeCount(oList_AttType);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), Count = countItem };
                }
                else
                {
                    var oList = dbSelector.get_Data_AttributeType(oList_AttType);
                    result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), OntologyItems = oList };
                }
                
                return result;
            }
            catch (Exception ex)
            {
                var oResult = Globals.LogStates.LogState_Error.Clone();
                oResult.Additional1 = ex.Message;
                var result = new WebServiceResult { Result = oResult };
                return result;
            }
            
        }

        [WebMethod]
        public clsOntologyItem GetOItem(string idItem, string type)
        {
            if (type == Globals.OTypes.ClassType)
            {
                var item = Classes(new List<clsOntologyItem> { new clsOntologyItem { GUID =  idItem } });
                return item.Result.GUID == Globals.LogStates.LogState_Success.GUID ? item.OntologyItems.FirstOrDefault() : item.Result;
            }
            else if (type == Globals.OTypes.ObjectType)
            {
                var item = Objects(new List<clsOntologyItem> { new clsOntologyItem { GUID = idItem } });
                return item.Result.GUID == Globals.LogStates.LogState_Success.GUID ? item.OntologyItems.FirstOrDefault() : item.Result;
            }
            else if (type == Globals.OTypes.AttributeType)
            {
                var item = AttributeTypes(new List<clsOntologyItem> { new clsOntologyItem { GUID = idItem } });
                return item.Result.GUID == Globals.LogStates.LogState_Success.GUID ? item.OntologyItems.FirstOrDefault() : item.Result;
            }
            else if (type == Globals.OTypes.RelationType)
            {
                var item = RelationTypes(new List<clsOntologyItem> { new clsOntologyItem { GUID = idItem } });
                return item.Result.GUID == Globals.LogStates.LogState_Success.GUID ? item.OntologyItems.FirstOrDefault() : item.Result;
            }
            else
            {
                return Globals.LogStates.LogState_Error.Clone();
            }
        }

        [WebMethod]
        public clsOntologyItem GetClassPath(string idClass)
        {
            return getClassPath(idClass, "");
        }
        
        private clsOntologyItem getClassPath(string idClass, string path)
        {
            var searchClass = new List<clsOntologyItem> { new clsOntologyItem { GUID = idClass } };

            

            try
            {
                var result = Classes(new List<clsOntologyItem> { new clsOntologyItem { GUID = idClass } });

                if (result.Result.GUID != Globals.LogStates.LogState_Success.GUID && result.OntologyItems.Any())
                {
                    
                    if (string.IsNullOrEmpty(path))
                    {
                        path = result.OntologyItems.First().Name;
                    }
                    else
                    {
                        path = result.OntologyItems.First().Name + "\\" + path;
                    }

                    if (!string.IsNullOrEmpty(result.OntologyItems.First().GUID_Parent))
                    {
                        return getClassPath(result.OntologyItems.First().GUID_Parent, path);
                    }
                    else
                    {
                        result.Result.Additional1 = path;
                        return result.Result;
                    }

                    
                }
                else
                {
                    return result.Result;
                }
            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }

            
        }

        [WebMethod]
        public clsOntologyItem DeleteIndex(string strIndex)
        {
            try
            {
                var indexResponse = dbSelector.ElConnector.DeleteIndex(d => dbSelector.GetDeleteIndexDescriptor().Index(strIndex));
                if (indexResponse.IsValid)
                {
                    return Globals.LogStates.LogState_Success.Clone();
                }
                else
                {
                    return Globals.LogStates.LogState_Error.Clone();
                }
            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
            

            
        }

        [WebMethod]
        public WebServiceResult IndexList(string server, int port)
        {
            try
            {
                var result = new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), IndexList = dbSelector.IndexList(server, port) };
                return result;

            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return new WebServiceResult { Result = Globals.LogStates.LogState_Error.Clone() };
                
            }
            
        }

        [WebMethod]
        public clsOntologyItem SaveDataTypes(List<clsOntologyItem> oList_DataTypes)
        {
            try
            {
                return dbUpdater.save_DataTypes(oList_DataTypes);
                
            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        [WebMethod]
        public clsOntologyItem SaveAttributeTypes(List<clsOntologyItem> oList_AttributeTypes)
        {
            try
            {
                var result = Globals.LogStates.LogState_Success.Clone();
                foreach (var oItem_AttributeType in oList_AttributeTypes)
                {
                    result = dbUpdater.save_AttributeType(oItem_AttributeType);
                    if (result.GUID == Globals.LogStates.LogState_Error.GUID)
                    {
                        break;
                    }

                }

                return result;
            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        [WebMethod]
        public clsOntologyItem SaveClasses(List<clsOntologyItem> oList_Classes)
        {
            try
            {
                var result = Globals.LogStates.LogState_Success.Clone();
                foreach (var oItem_Class in oList_Classes)
	            {
                    result = dbUpdater.save_Class(oItem_Class);    
	            }

                return result;

            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        [WebMethod]
        public clsOntologyItem SaveClassAtts(List<clsClassAtt> oList_ClassAtts)
        {
            try
            {

                return dbUpdater.save_ClassAtt(oList_ClassAtts);

            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        [WebMethod]
        public clsOntologyItem SaveClassRels(List<clsClassRel> oList_ClassRels)
        {
            try
            {

                return dbUpdater.save_ClassRel(oList_ClassRels);

            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        [WebMethod]
        public WebServiceResult SaveObjectAttributes(List<clsObjectAtt> oList_ObjAtts)
        {
            try
            {

                return new WebServiceResult { Result = Globals.LogStates.LogState_Success.Clone(), ObjectAttributes = dbUpdater.save_ObjectAtt(oList_ObjAtts) };

            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return new WebServiceResult { Result = result };
            }
        }

        [WebMethod]
        public clsOntologyItem SaveObjects(List<clsOntologyItem> oList_Objects)
        {
            try
            {

                return dbUpdater.save_Objects(oList_Objects);

            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        [WebMethod]
        public clsOntologyItem SaveObjectRels(List<clsObjectRel> oList_ObjRels)
        {
            try
            {

                return dbUpdater.save_ObjectRel(oList_ObjRels);

            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        [WebMethod]
        public clsOntologyItem SaveRelationTypes(List<clsOntologyItem> oList_RelationTypes)
        {
            try
            {
                var result = Globals.LogStates.LogState_Success.Clone();
                foreach (var oItem_RelationType in oList_RelationTypes)
                {
                    result = dbUpdater.save_RelationType(oItem_RelationType);
                    if (result.GUID == Globals.LogStates.LogState_Error.GUID)
                    {
                        break;
                    }

                }

                return result;
            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        [WebMethod]
        public clsOntologyItem DeleteAttributeTypes(List<clsOntologyItem> oList_AttributeTypes)
        {
            try
            {
                return dbDeletor.del_AttributeType(oList_AttributeTypes);

            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
            
        }

        [WebMethod]
        public clsOntologyItem DeleteClasses(List<clsOntologyItem> oList_Classes)
        {
            try
            {
                return dbDeletor.del_Class(oList_Classes);

            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }

        }

        [WebMethod]
        public clsOntologyItem DeleteClassAttType(clsOntologyItem oItem_Class, clsOntologyItem oItem_AttributeType)
        {
            try
            {
                return dbDeletor.del_ClassAttType(oItem_Class, oItem_AttributeType);

            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        [WebMethod]
        public clsOntologyItem DeleteClassRel(List<clsClassRel> oList_ClassRel)
        {
            try
            {
                return dbDeletor.del_ClassRel(oList_ClassRel);

            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        [WebMethod]
        public clsOntologyItem DeleteDataTypes(List<clsOntologyItem> oList_DataTypes)
        {
            try
            {
                return dbDeletor.del_DataType(oList_DataTypes);

            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        [WebMethod]
        public clsOntologyItem DeleteObjectAttributes(List<clsObjectAtt> oList_ObjectAttributes)
        {
            try
            {
                return dbDeletor.del_ObjectAtt(oList_ObjectAttributes);

            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        [WebMethod]
        public clsOntologyItem DeleteObjectRelations(List<clsObjectRel> oList_ObjectRelations)
        {
            try
            {
                return dbDeletor.del_ObjectRel(oList_ObjectRelations);

            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        [WebMethod]
        public clsOntologyItem DeleteObjects(List<clsOntologyItem> oList_Objects)
        {
            try
            {
                return dbDeletor.del_Objects(oList_Objects);

            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        [WebMethod]
        public clsOntologyItem DeleteRelationTypes(List<clsOntologyItem> oList_RelationTypes)
        {
            try
            {
                return dbDeletor.del_RelationType(oList_RelationTypes);

            }
            catch (Exception ex)
            {
                var result = Globals.LogStates.LogState_Error.Clone();
                result.Additional1 = ex.Message;
                return result;
            }
        }

        [WebMethod]
        public bool CreateIndex()
        {
            var indexSettings = dbSelector.GetIndexSettings();
            var objOPResult = dbSelector.ElConnector.CreateIndex(Globals.ElIndex);
            return objOPResult.IsValid;
        }

        [WebMethod]
        public bool TestIndexExistance()
        {
            var objIndexDescriptor = dbSelector.GetIndexExistsDescriptor();
            objIndexDescriptor.Index(Globals.ElIndex);

            return dbSelector.ElConnector.IndexExists(f => objIndexDescriptor).Exists;
        }

        [WebMethod]
        public string Field_ID_Object()
        {
            return fields.ID_Object;
        }

        [WebMethod]
        public string Field_ID_Item()
        {
            return fields.ID_Item;

        }

        [WebMethod]
        public string Field_ID_Class_Left()
        {
            return fields.ID_Class_Left;

        }

        [WebMethod]
        public string Field_ID_Class_Right()
        {
            return fields.ID_Class_Right;

        }

        [WebMethod]
        public string Field_Max_forw()
        {
            return fields.Max_Forw;

        }

        [WebMethod]
        public string Field_Min_forw()
        {
            return fields.Min_Forw;

        }

        [WebMethod]
        public string Field_Min()
        {
            return fields.Min;

        }

        [WebMethod]
        public string Field_Max()
        {
            return fields.Max;

        }

        [WebMethod]
        public string Field_Max_backw()
        {
            return fields.Max_Backw;

        }

        [WebMethod]
        public string Field_ID_AttributeType()
        {
            return fields.ID_AttributeType;

        }

        [WebMethod]
        public string Field_ID_Class()
        {
            return fields.ID_Class;

        }

        [WebMethod]
        public string Field_ID_DataType()
        {
            return fields.ID_DataType;

        }

        [WebMethod]
        public string Field_Ontology()
        {
            return fields.Ontology;

        }

        [WebMethod]
        public string Field_ID_Parent()
        {
            return fields.ID_Parent;

        }

        [WebMethod]
        public string Field_ID_Parent_Object()
        {
            return fields.ID_Parent_Object;

        }

        [WebMethod]
        public string Field_ID_Parent_Other()
        {
            return fields.ID_Parent_Other;

        }

        [WebMethod]
        public string Field_ID_RelationType()
        {
            return fields.ID_RelationType;

        }

        [WebMethod]
        public string Field_ID_Other()
        {
            return fields.ID_Other;

        }

        [WebMethod]
        public string Field_Name_AttributeType()
        {
            return fields.Name_AttributeType;

        }

        [WebMethod]
        public string Field_Name_Object()
        {
            return fields.Name_Object;

        }

        [WebMethod]
        public string Field_Name_Other()
        {
            return fields.Name_Other;

        }

        [WebMethod]
        public string Field_Name_Item()
        {
            return fields.Name_Item;

        }

        [WebMethod]
        public string Field_Name_RelationType()
        {
            return fields.Name_RelationType;

        }

        [WebMethod]
        public string Field_OrderID()
        {
            return fields.OrderID;

        }

        [WebMethod]
        public string Field_Val_Bool()
        {
            return fields.Val_Bool;

        }

        [WebMethod]
        public string Field_Val_Datetime()
        {
            return fields.Val_Datetime;

        }

        [WebMethod]
        public string Field_Val_Int()
        {
            return fields.Val_Int;

        }

        [WebMethod]
        public string Field_Val_Real()
        {
            return fields.Val_Real;

        }

        [WebMethod]
        public string Field_Val_String()
        {
            return fields.Val_String;

        }

        [WebMethod]
        public string Field_Val_Name()
        {
            return fields.Val_Name;

        }

        [WebMethod]
        public string Field_ID_Attribute()
        {
            return fields.ID_Attribute;

        }
    }
}
