﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OntWeb
{
    public class Config
    {
        public string ConfigItem { get; set; }
        public string ConfigValueString { get; set; }
        public int ConfigValueInt { get; set; }
    }
}